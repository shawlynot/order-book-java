package com.shawlynot.orderbook.listener;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.shawlynot.orderbook.book.Book;
import com.shawlynot.orderbook.model.L2Update;
import com.shawlynot.orderbook.model.Snapshot;
import org.springframework.stereotype.Component;

@Component
public class MessageHandler {

    private final Book book;
    private final ObjectMapper mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

    public MessageHandler(Book book) {
        this.book = book;
    }

    public void handleMessage(String message) throws JsonProcessingException {
        var json = mapper.readTree(message);
        var type = json.get("type").asText();
        switch (type) {
            case "subscriptions" -> System.out.println(message);
            case "snapshot" -> book.resetFromSnapshot(mapper.readValue(message, Snapshot.class));
            case "l2update" -> book.update(mapper.readValue(message, L2Update.class));
        }
    }
}
