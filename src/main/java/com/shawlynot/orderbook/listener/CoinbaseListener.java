package com.shawlynot.orderbook.listener;

import org.springframework.stereotype.Component;

import java.net.http.WebSocket;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

@Component
public class CoinbaseListener implements WebSocket.Listener {

    private StringBuffer accumulatedMessage = new StringBuffer();
    private CompletableFuture<String> future = new CompletableFuture<>();

    private final MessageHandler handler;

    public CoinbaseListener(MessageHandler handler) {
        this.handler = handler;
    }

    @Override
    public CompletionStage<?> onText(WebSocket webSocket, CharSequence data, boolean last) {
        try {
            accumulatedMessage.append(data);
            if (last) {
                handler.handleMessage(accumulatedMessage.toString());
                accumulatedMessage = new StringBuffer();
            }
            webSocket.request(1); //request next message
            return CompletableFuture.completedStage(data);

        } catch (Exception e) {
            future.completeExceptionally(e);
            return CompletableFuture.failedStage(e);
        }
    }

    public CompletableFuture<String> waitForError(){
        return future;
    }
}
