package com.shawlynot.orderbook.run;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.shawlynot.orderbook.listener.CoinbaseListener;
import com.shawlynot.orderbook.model.Subscribe;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.WebSocket;
import java.util.List;

@Component
public class Runner implements CommandLineRunner {

    private final CoinbaseListener listener;

    public Runner(CoinbaseListener listener) {
        this.listener = listener;
    }

    @Override
    public void run(String... args) throws Exception {
        var pair = args[0];
        var client = HttpClient.newHttpClient()
                .newWebSocketBuilder()
                .buildAsync(URI.create("wss://ws-feed-public.sandbox.pro.coinbase.com"), listener)
                .get();

        var mapper = new ObjectMapper();
        var subscription = Subscribe.getL2Subscription(List.of(pair));
        String subscriptionMessage = mapper.writeValueAsString(subscription);
        client.sendText(subscriptionMessage, true);

        listener.waitForError().get();
        client.sendClose(WebSocket.NORMAL_CLOSURE, "disconnect").thenRun(() -> System.out.println("DISCONNECTED"));
    }
}
