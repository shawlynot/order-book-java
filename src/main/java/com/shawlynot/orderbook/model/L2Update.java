package com.shawlynot.orderbook.model;

import java.util.List;

public record L2Update(String type, String product_id, List<List<String>> changes) {
}
