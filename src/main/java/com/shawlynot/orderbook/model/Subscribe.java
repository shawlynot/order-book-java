package com.shawlynot.orderbook.model;

import java.util.List;

public record Subscribe(String type, List<String> product_ids, List<String> channels) {
    public static Subscribe getL2Subscription(List<String> pairs){
        return new Subscribe("subscribe", pairs, List.of("level2"));
    }
}

