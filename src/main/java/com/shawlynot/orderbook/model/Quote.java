package com.shawlynot.orderbook.model;

public record Quote(double price, double quantity) implements Comparable<Quote> {

    @Override
    public int compareTo(Quote that) {
        return Double.compare(price, that.price);
    }
}
