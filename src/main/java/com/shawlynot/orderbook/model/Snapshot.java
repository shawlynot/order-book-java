package com.shawlynot.orderbook.model;

import java.util.List;

public record Snapshot(String type, String product_id, List<List<String>> asks, List<List<String>> bids) {
}
