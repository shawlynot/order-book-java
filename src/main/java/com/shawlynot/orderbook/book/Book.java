package com.shawlynot.orderbook.book;

import com.shawlynot.orderbook.model.L2Update;
import com.shawlynot.orderbook.model.Quote;
import com.shawlynot.orderbook.model.Snapshot;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Component
public class Book {

    private List<Quote> orderBookBids = new ArrayList<>();
    private List<Quote> orderBookAsks = new ArrayList<>();

    public void resetFromSnapshot(Snapshot snapshot) {
        var snapshotBids = snapshot.bids().stream()
                .map(this::getQuote)
                .toList();
        orderBookBids = sortBidsDescending(snapshotBids);

        var snapshotAsks = snapshot.asks().stream()
                .map(this::getQuote)
                .toList();
        orderBookAsks = sortAsksAscending(snapshotAsks);

        printOrderBook();
    }

    public void update(L2Update update) {
        update.changes().forEach(change -> {
            var changeType = change.get(0);
            var quote = getQuote(List.of(change.get(1), change.get(2)));
            if (quote.quantity() == 0) {
                if (changeType.equals("buy")) {
                    orderBookBids = orderBookBids.stream()
                            .filter(bid -> bid.price() != quote.price())
                            .toList();
                } else {
                    orderBookAsks = orderBookAsks.stream()
                            .filter(ask -> ask.price() != quote.price())
                            .toList();
                }
            } else {
                if (changeType.equals("buy")) {
                    var newBids = new ArrayList<>(orderBookBids);
                    newBids.add(quote);
                    orderBookBids = sortBidsDescending(newBids);
                } else {
                    var newAsks = new ArrayList<>(orderBookAsks);
                    newAsks.add(quote);
                    orderBookAsks = sortAsksAscending(newAsks);
                }
            }
        });
        printOrderBook();
    }

    private static List<Quote> sortBidsDescending(List<Quote> bids) {
        ArrayList<Quote> out = new ArrayList<>(bids);
        Collections.sort(out);
        Collections.reverse(out);
        return out;
    }

    private static List<Quote> sortAsksAscending(List<Quote> asks) {
        ArrayList<Quote> out = new ArrayList<>(asks);
        Collections.sort(out);
        return out;
    }

    private Quote getQuote(List<String> quote) {
        return new Quote(
                Double.parseDouble(quote.get(0)),
                Double.parseDouble(quote.get(1))
        );
    }

    private void printOrderBook() {
        System.out.println("=====Bids, [price, size], highest first)=====");
        orderBookBids.stream().limit(10).toList().forEach(bid ->
                System.out.printf("[%f, %f]%n", bid.price(), bid.quantity()));
        System.out.println("=====Asks, [price, size], (lowest first)=====");
        orderBookAsks.stream().limit(10).toList().forEach(ask ->
                System.out.printf("[%f, %f]%n", ask.price(), ask.quantity()));
        System.out.println("=============================================");
    }
}
